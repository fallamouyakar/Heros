const mongoose = require('mongoose');

const TaskListSchema = new mongoose.Schema({
    title: {
        type: String,
        required: true,
        minlength: 1,
        trim: true
    }
});

const List = mongoose.model('TaskList', TaskListSchema);

module.exports = { TaskList };