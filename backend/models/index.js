const { List } = require('./TaskList');
const { Task } = require('./Task');

module.exports = {
    TaskList,
    Task
};